package com.tsconsulting.dsubbotin.tm.command.data.jaxb;

import com.tsconsulting.dsubbotin.tm.command.AbstractCommand;
import com.tsconsulting.dsubbotin.tm.endpoint.Session;
import com.tsconsulting.dsubbotin.tm.util.TerminalUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public final class DataJsonSaveJaxbCommand extends AbstractCommand {

    @Override
    public @NotNull String name() {
        return "data-save-json-jaxb";
    }

    @Override
    public @NotNull String description() {
        return "Data saved in JSON using JAXB.";
    }

    @Override
    public void execute() {
        @Nullable Session session = endpointLocator.getSessionService().getSession();
        endpointLocator.getAdminEndpoint().dataSaveJsonJaxb(session);
        TerminalUtil.printMessage("Save to JSON completed.");
    }
}
