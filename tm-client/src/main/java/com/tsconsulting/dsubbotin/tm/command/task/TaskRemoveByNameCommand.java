package com.tsconsulting.dsubbotin.tm.command.task;

import com.tsconsulting.dsubbotin.tm.command.AbstractTaskCommand;
import com.tsconsulting.dsubbotin.tm.endpoint.Session;
import com.tsconsulting.dsubbotin.tm.util.TerminalUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public final class TaskRemoveByNameCommand extends AbstractTaskCommand {

    @Override
    @NotNull
    public String name() {
        return "task-remove-by-name";
    }

    @Override
    @NotNull
    public String description() {
        return "Remove task by name.";
    }

    @Override
    public void execute() throws Exception {
        @Nullable Session session = endpointLocator.getSessionService().getSession();
        TerminalUtil.printMessage("Enter name:");
        @NotNull final String name = TerminalUtil.nextLine();
        endpointLocator.getTaskEndpoint().removeByNameTask(session, name);
        TerminalUtil.printMessage("[Task removed]");
    }

}
