package com.tsconsulting.dsubbotin.tm.api.service;

import com.tsconsulting.dsubbotin.tm.endpoint.*;
import org.jetbrains.annotations.NotNull;

public interface IEndpointLocator {

    @NotNull
    SessionEndpoint getSessionEndpoint();

    @NotNull
    UserEndpoint getUserEndpoint();

    @NotNull
    ProjectEndpoint getProjectEndpoint();

    @NotNull
    ProjectTaskEndpoint getProjectTaskEndpoint();

    @NotNull
    TaskEndpoint getTaskEndpoint();

    @NotNull
    AdminEndpoint getAdminEndpoint();

    @NotNull
    AdminUserEndpoint getAdminUserEndpoint();

    @NotNull
    IPropertyService getPropertyService();

    @NotNull
    ICommandService getCommandService();

    @NotNull
    ISessionService getSessionService();

}
