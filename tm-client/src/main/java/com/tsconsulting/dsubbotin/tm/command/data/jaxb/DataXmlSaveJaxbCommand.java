package com.tsconsulting.dsubbotin.tm.command.data.jaxb;

import com.tsconsulting.dsubbotin.tm.command.AbstractCommand;
import com.tsconsulting.dsubbotin.tm.endpoint.Session;
import com.tsconsulting.dsubbotin.tm.util.TerminalUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public final class DataXmlSaveJaxbCommand extends AbstractCommand {

    @Override
    public @NotNull String name() {
        return "data-save-xml-jaxb";
    }

    @Override
    public @NotNull String description() {
        return "Data saved in XML using JAXB.";
    }

    @Override
    public void execute() {
        @Nullable Session session = endpointLocator.getSessionService().getSession();
        endpointLocator.getAdminEndpoint().dataSaveXmlJaxb(session);
        TerminalUtil.printMessage("Save to XML completed.");
    }

}
