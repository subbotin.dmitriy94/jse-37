package com.tsconsulting.dsubbotin.tm.service;

import com.tsconsulting.dsubbotin.tm.api.repository.IProjectRepository;
import com.tsconsulting.dsubbotin.tm.api.repository.ITaskRepository;
import com.tsconsulting.dsubbotin.tm.api.service.IConnectionService;
import com.tsconsulting.dsubbotin.tm.api.service.ILogService;
import com.tsconsulting.dsubbotin.tm.api.service.IProjectTaskService;
import com.tsconsulting.dsubbotin.tm.exception.AbstractException;
import com.tsconsulting.dsubbotin.tm.exception.empty.EmptyIdException;
import com.tsconsulting.dsubbotin.tm.exception.empty.EmptyNameException;
import com.tsconsulting.dsubbotin.tm.exception.entity.ProjectNotFoundException;
import com.tsconsulting.dsubbotin.tm.exception.entity.TaskNotFoundException;
import com.tsconsulting.dsubbotin.tm.exception.system.DatabaseOperationException;
import com.tsconsulting.dsubbotin.tm.exception.system.IndexIncorrectException;
import com.tsconsulting.dsubbotin.tm.model.Project;
import com.tsconsulting.dsubbotin.tm.model.Task;
import com.tsconsulting.dsubbotin.tm.repository.ProjectRepository;
import com.tsconsulting.dsubbotin.tm.repository.TaskRepository;
import com.tsconsulting.dsubbotin.tm.util.EmptyUtil;
import lombok.RequiredArgsConstructor;
import org.jetbrains.annotations.NotNull;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

@RequiredArgsConstructor
public final class ProjectTaskService implements IProjectTaskService {

    @NotNull
    private final IConnectionService connectionService;

    @NotNull
    private final ILogService logService;

    @Override
    public void bindTaskToProject(
            @NotNull final String userId,
            @NotNull final String projectId,
            @NotNull final String taskId
    ) throws AbstractException {
        try (@NotNull final Connection connection = connectionService.getConnection()) {
            @NotNull final IProjectRepository projectRepository = new ProjectRepository(connection);
            @NotNull final ITaskRepository taskRepository = new TaskRepository(connection);
            isEmpty(projectRepository, taskRepository, userId, projectId, taskId);
            taskRepository.bindTaskToProjectById(userId, projectId, taskId);
            connection.commit();
        } catch (SQLException exception) {
            logService.error(exception);
            throw new DatabaseOperationException();
        }
    }

    @Override
    public void unbindTaskFromProject(
            @NotNull final String userId,
            @NotNull final String projectId,
            @NotNull final String taskId
    ) throws AbstractException {
        try (@NotNull final Connection connection = connectionService.getConnection()) {
            @NotNull final IProjectRepository projectRepository = new ProjectRepository(connection);
            @NotNull final ITaskRepository taskRepository = new TaskRepository(connection);
            isEmpty(projectRepository, taskRepository, userId, projectId, taskId);
            taskRepository.unbindTaskById(userId, taskId);
            connection.commit();
        } catch (SQLException exception) {
            logService.error(exception);
            throw new DatabaseOperationException();
        }
    }

    @Override
    @NotNull
    public List<Task> findAllTasksByProjectId(
            @NotNull final String userId,
            @NotNull final String id
    ) throws AbstractException {
        if (EmptyUtil.isEmpty(id)) throw new EmptyIdException();
        try (@NotNull final Connection connection = connectionService.getConnection()) {
            @NotNull final IProjectRepository projectRepository = new ProjectRepository(connection);
            if (!projectRepository.existById(userId, id)) throw new ProjectNotFoundException();
            @NotNull final ITaskRepository taskRepository = new TaskRepository(connection);
            return taskRepository.findAllByProjectId(userId, id);
        } catch (SQLException exception) {
            logService.error(exception);
            throw new DatabaseOperationException();
        }
    }

    @Override
    public void removeProjectById(
            @NotNull final String userId,
            @NotNull final String id
    ) throws AbstractException {
        if (EmptyUtil.isEmpty(id)) throw new EmptyIdException();
        try (@NotNull final Connection connection = connectionService.getConnection()) {
            @NotNull final IProjectRepository projectRepository = new ProjectRepository(connection);
            if (!projectRepository.existById(userId, id)) throw new ProjectNotFoundException();
            @NotNull final ITaskRepository taskRepository = new TaskRepository(connection);
            taskRepository.removeAllTaskByProjectId(userId, id);
            projectRepository.removeById(userId, id);
            connection.commit();
        } catch (SQLException exception) {
            logService.error(exception);
            throw new DatabaseOperationException();
        }
    }

    @Override
    public void removeProjectByIndex(
            @NotNull final String userId,
            final int index
    ) throws AbstractException {
        if (index < 0) throw new IndexIncorrectException();
        try (@NotNull final Connection connection = connectionService.getConnection()) {
            @NotNull final IProjectRepository projectRepository = new ProjectRepository(connection);
            @NotNull final Project project = projectRepository.findByIndex(userId, index);
            @NotNull final ITaskRepository taskRepository = new TaskRepository(connection);
            taskRepository.removeAllTaskByProjectId(userId, project.getId());
            projectRepository.removeByIndex(userId, index);
            connection.commit();
        } catch (SQLException exception) {
            logService.error(exception);
            throw new DatabaseOperationException();
        }
    }

    @Override
    public void removeProjectByName(
            @NotNull final String userId,
            @NotNull final String name
    ) throws AbstractException {
        if (EmptyUtil.isEmpty(name)) throw new EmptyNameException();
        try (@NotNull final Connection connection = connectionService.getConnection()) {
            @NotNull final IProjectRepository projectRepository = new ProjectRepository(connection);
            @NotNull final ITaskRepository taskRepository = new TaskRepository(connection);
            @NotNull final Project project = projectRepository.findByName(userId, name);
            taskRepository.removeAllTaskByProjectId(userId, project.getId());
            projectRepository.removeByName(userId, name);
            connection.commit();
        } catch (SQLException exception) {
            logService.error(exception);
            throw new DatabaseOperationException();
        }
    }

    @Override
    public void removeAllProject(@NotNull String userId) throws AbstractException {
        try (@NotNull final Connection connection = connectionService.getConnection()) {
            @NotNull final IProjectRepository projectRepository = new ProjectRepository(connection);
            @NotNull final ITaskRepository taskRepository = new TaskRepository(connection);
            @NotNull List<Project> projects = projectRepository.findAll(userId);
            for (Project project : projects) taskRepository.removeAllTaskByProjectId(userId, project.getId());
            projectRepository.clear(userId);
            connection.commit();
        } catch (SQLException exception) {
            logService.error(exception);
            throw new DatabaseOperationException();
        }
    }

    private void isEmpty(
            @NotNull final IProjectRepository projectRepository,
            @NotNull final ITaskRepository taskRepository,
            @NotNull final String userId,
            @NotNull final String projectId,
            @NotNull final String taskId
    ) throws AbstractException {
        if (EmptyUtil.isEmpty(projectId)) throw new EmptyIdException();
        if (EmptyUtil.isEmpty(taskId)) throw new EmptyIdException();
        if (!projectRepository.existById(userId, projectId)) throw new ProjectNotFoundException();
        if (!taskRepository.existById(userId, taskId)) throw new TaskNotFoundException();
    }

}
