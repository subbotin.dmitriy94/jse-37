package com.tsconsulting.dsubbotin.tm.api.endpoint;

import com.tsconsulting.dsubbotin.tm.exception.AbstractException;
import com.tsconsulting.dsubbotin.tm.model.Session;
import org.jetbrains.annotations.NotNull;

import javax.jws.WebMethod;
import javax.jws.WebParam;

public interface ISessionEndpoint {

    @NotNull
    @WebMethod
    Session openSession(
            @NotNull @WebParam(name = "login") String login,
            @NotNull @WebParam(name = "password") String password
    ) throws AbstractException;

    @WebMethod
    boolean closeSession(
            @NotNull @WebParam(name = "session") Session session
    ) throws AbstractException;

    @NotNull
    @WebMethod
    Session register(
            @NotNull @WebParam(name = "login") String login,
            @NotNull @WebParam(name = "password") String password,
            @NotNull @WebParam(name = "email") String email
    ) throws AbstractException;

}
