package com.tsconsulting.dsubbotin.tm.api.service;

import com.tsconsulting.dsubbotin.tm.exception.AbstractException;
import com.tsconsulting.dsubbotin.tm.model.AbstractEntity;
import org.jetbrains.annotations.NotNull;

import java.util.Comparator;
import java.util.List;

public interface IService<E extends AbstractEntity> {

    void remove(@NotNull E entity) throws AbstractException;

    void clear() throws AbstractException;

    @NotNull
    List<E> findAll() throws AbstractException;

    @NotNull
    List<E> findAll(@NotNull Comparator<E> comparator) throws AbstractException;

    @NotNull
    E findById(@NotNull String id) throws AbstractException;

    @NotNull
    E findByIndex(int index) throws AbstractException;

    void removeById(@NotNull String id) throws AbstractException;

    void removeByIndex(int index) throws AbstractException;

}