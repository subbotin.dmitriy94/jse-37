package com.tsconsulting.dsubbotin.tm.repository;

import com.tsconsulting.dsubbotin.tm.api.repository.IProjectRepository;
import com.tsconsulting.dsubbotin.tm.enumerated.Status;
import com.tsconsulting.dsubbotin.tm.exception.AbstractException;
import com.tsconsulting.dsubbotin.tm.exception.entity.ProjectNotFoundException;
import com.tsconsulting.dsubbotin.tm.model.Project;
import com.tsconsulting.dsubbotin.tm.util.EnumerationUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.sql.*;
import java.util.Date;

public final class ProjectRepository extends AbstractOwnerRepository<Project> implements IProjectRepository {

    public ProjectRepository(@NotNull Connection connection) {
        super(connection);
    }

    @NotNull
    @Override
    public Project fetch(final @NotNull ResultSet row) throws AbstractException, SQLException {
        @NotNull final Project project = new Project();
        project.setId(row.getString("id"));
        project.setUserId(row.getString("user_id"));
        project.setName(row.getString("name"));
        project.setDescription(row.getString("description"));
        project.setStatus(EnumerationUtil.parseStatus(row.getString("status")));
        project.setCreateDate(row.getTimestamp("create_date"));
        project.setStartDate(row.getTimestamp("start_date"));
        return project;
    }

    @NotNull
    @Override
    public String getTableName() {
        return "projects";
    }

    @Override
    public void add(
            @NotNull String userId,
            @NotNull Project project
    ) throws AbstractException, SQLException {
        @NotNull final String query = "INSERT INTO " + getTableName() +
                " (id, name, description, status, create_date, start_date, user_id) " +
                "VALUES (?, ?, ?, ?, ?, ?, ?);";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, project.getId());
        statement.setString(2, project.getName());
        statement.setString(3, project.getDescription());
        statement.setString(4, project.getStatus().toString());
        statement.setTimestamp(5, new Timestamp(project.getCreateDate().getTime()));
        @Nullable final Date startDate = project.getStartDate();
        statement.setTimestamp(6, startDate == null ? null : new Timestamp(startDate.getTime()));
        statement.setString(7, project.getUserId());
        statement.executeUpdate();
        statement.close();
    }

    @Override
    @NotNull
    public Project findByName(
            @NotNull final String userId,
            @NotNull final String name
    ) throws AbstractException, SQLException {
        @NotNull final String query = "SELECT * FROM " + getTableName() +
                " WHERE user_id = ? AND name = ?;";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, userId);
        statement.setString(2, name);
        @NotNull final ResultSet resultSet = statement.executeQuery();
        if (!resultSet.next()) throw new ProjectNotFoundException();
        @NotNull final Project project = fetch(resultSet);
        statement.close();
        return project;
    }

    @Override
    public void removeByName(
            @NotNull final String userId,
            @NotNull final String name
    ) throws AbstractException, SQLException {
        @NotNull final String query = "DELETE FROM " + getTableName() +
                " WHERE user_id = ? and name = ?;";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, userId);
        statement.setString(2, name);
        statement.executeUpdate();
        statement.close();
    }

    @Override
    public void updateById(
            @NotNull final String userId,
            @NotNull final String id,
            @NotNull final String name,
            @NotNull final String description
    ) throws AbstractException, SQLException {
        @NotNull final String query = "UPDATE " + getTableName() +
                " SET name = ?, description = ?" +
                " WHERE user_id = ? and id = ?;";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, name);
        statement.setString(2, description);
        statement.setString(3, userId);
        statement.setString(4, id);
        statement.executeUpdate();
        statement.close();
    }

    @Override
    public void updateByIndex(
            @NotNull final String userId,
            final int index,
            @NotNull final String name,
            @NotNull final String description
    ) throws AbstractException, SQLException {
        @NotNull final Project project = findByIndex(userId, index);
        @NotNull final String projectId = project.getId();
        @NotNull final String query = "UPDATE " + getTableName() +
                " SET name = ?, description = ?" +
                " WHERE user_id = ? and id = ?;";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, name);
        statement.setString(2, description);
        statement.setString(3, userId);
        statement.setString(4, projectId);
        statement.executeUpdate();
        statement.close();
    }

    @Override
    public void startById(@NotNull final String userId, @NotNull final String id) throws AbstractException, SQLException {
        @NotNull final String query = "UPDATE " + getTableName() +
                " SET status = ?, start_date = ?" +
                " WHERE user_id = ? and id = ?;";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, Status.IN_PROGRESS.toString());
        statement.setTimestamp(2, new Timestamp(new Date().getTime()));
        statement.setString(3, userId);
        statement.setString(4, id);
        statement.executeUpdate();
        statement.close();
    }

    @Override
    public void startByIndex(@NotNull final String userId, final int index) throws AbstractException, SQLException {
        @NotNull final Project project = findByIndex(userId, index);
        @NotNull final String projectId = project.getId();
        @NotNull final String query = "UPDATE " + getTableName() +
                " SET status = ?, start_date = ?" +
                " WHERE user_id = ? and id = ?;";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, Status.IN_PROGRESS.toString());
        statement.setTimestamp(2, new Timestamp(new Date().getTime()));
        statement.setString(3, userId);
        statement.setString(4, projectId);
        statement.executeUpdate();
        statement.close();
    }

    @Override
    public void startByName(@NotNull final String userId, @NotNull final String name) throws AbstractException, SQLException {
        @NotNull final String query = "UPDATE " + getTableName() +
                " SET status = ?, start_date = ?" +
                " WHERE user_id = ? and name = ?;";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, Status.IN_PROGRESS.toString());
        statement.setTimestamp(2, new Timestamp(new Date().getTime()));
        statement.setString(3, userId);
        statement.setString(4, name);
        statement.executeUpdate();
        statement.close();
    }

    @Override
    public void finishById(
            @NotNull final String userId,
            @NotNull final String id
    ) throws AbstractException, SQLException {
        @NotNull final String query = "UPDATE " + getTableName() +
                " SET status = ?" +
                " WHERE user_id = ? and id = ?;";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, Status.COMPLETED.toString());
        statement.setString(2, userId);
        statement.setString(3, id);
        statement.executeUpdate();
        statement.close();
    }

    @Override
    public void finishByIndex(
            @NotNull final String userId,
            final int index
    ) throws AbstractException, SQLException {
        @NotNull final Project project = findByIndex(userId, index);
        @NotNull final String projectId = project.getId();
        @NotNull final String query = "UPDATE " + getTableName() +
                " SET status = ?" +
                " WHERE user_id = ? AND id = ?;";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, Status.COMPLETED.toString());
        statement.setString(2, userId);
        statement.setString(3, projectId);
        statement.executeUpdate();
        statement.close();
    }

    @Override
    public void finishByName(
            @NotNull final String userId,
            @NotNull final String name
    ) throws AbstractException, SQLException {
        @NotNull final String query = "UPDATE " + getTableName() +
                " SET status = ?" +
                " WHERE user_id = ? and name = ?;";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, Status.COMPLETED.toString());
        statement.setString(2, userId);
        statement.setString(3, name);
        statement.executeUpdate();
        statement.close();
    }

    @Override
    public void updateStatusById(
            @NotNull final String userId,
            @NotNull final String id,
            @NotNull final Status status
    ) throws AbstractException, SQLException {
        @NotNull final String subQueryUpdate = status.compareTo(Status.IN_PROGRESS) > 0 ?
                ", start_date = current_timestamp" : "";
        @NotNull final String query = "UPDATE " + getTableName() +
                " SET status = ? " + subQueryUpdate +
                " WHERE user_id = ? and id = ?;";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, status.toString());
        statement.setString(2, userId);
        statement.setString(3, id);
        statement.executeUpdate();
        statement.close();
    }

    @Override
    public void updateStatusByIndex(
            @NotNull final String userId,
            final int index,
            @NotNull final Status status
    ) throws AbstractException, SQLException {
        @NotNull final Project project = findByIndex(userId, index);
        @NotNull final String projectId = project.getId();
        @NotNull final String subQueryUpdate = status.compareTo(Status.IN_PROGRESS) > 0 ?
                ", start_date = current_timestamp" : "";
        @NotNull final String query = "UPDATE " + getTableName() +
                " SET status = ? " + subQueryUpdate +
                " WHERE user_id = ? and id = ?;";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, status.toString());
        statement.setString(2, userId);
        statement.setString(3, projectId);
        statement.executeUpdate();
        statement.close();
    }

    @Override
    public void updateStatusByName(
            @NotNull final String userId,
            @NotNull final String name,
            @NotNull final Status status
    ) throws AbstractException, SQLException {
        @NotNull final String subQueryUpdate = status.compareTo(Status.IN_PROGRESS) > 0 ?
                ", start_date = current_timestamp" : "";
        @NotNull final String query = "UPDATE " + getTableName() +
                " SET status = ? " + subQueryUpdate +
                " WHERE user_id = ? and name = ?;";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, status.toString());
        statement.setString(2, userId);
        statement.setString(3, name);
        statement.executeUpdate();
        statement.close();
    }

}