package com.tsconsulting.dsubbotin.tm.service;

import com.tsconsulting.dsubbotin.tm.api.repository.IRepository;
import com.tsconsulting.dsubbotin.tm.api.service.IConnectionService;
import com.tsconsulting.dsubbotin.tm.api.service.ILogService;
import com.tsconsulting.dsubbotin.tm.api.service.IService;
import com.tsconsulting.dsubbotin.tm.exception.AbstractException;
import com.tsconsulting.dsubbotin.tm.exception.empty.EmptyIdException;
import com.tsconsulting.dsubbotin.tm.exception.system.DatabaseOperationException;
import com.tsconsulting.dsubbotin.tm.exception.system.IndexIncorrectException;
import com.tsconsulting.dsubbotin.tm.model.AbstractEntity;
import com.tsconsulting.dsubbotin.tm.util.EmptyUtil;
import lombok.AllArgsConstructor;
import org.jetbrains.annotations.NotNull;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Comparator;
import java.util.List;

@AllArgsConstructor
public abstract class AbstractService<E extends AbstractEntity> implements IService<E> {

    @NotNull
    protected final IConnectionService connectionService;

    @NotNull
    protected final ILogService logService;

    @NotNull
    protected abstract IRepository<E> getRepository(@NotNull final Connection connection);

    @Override
    public void remove(@NotNull final E entity) throws AbstractException {
        try (@NotNull final Connection connection = connectionService.getConnection()) {
            @NotNull final IRepository<E> repository = getRepository(connection);
            repository.remove(entity);
            connection.commit();
        } catch (SQLException exception) {
            logService.error(exception);
            throw new DatabaseOperationException();
        }
    }

    @Override
    public void clear() throws AbstractException {
        try (@NotNull final Connection connection = connectionService.getConnection()) {
            @NotNull final IRepository<E> repository = getRepository(connection);
            repository.clear();
            connection.commit();
        } catch (SQLException exception) {
            logService.error(exception);
            throw new DatabaseOperationException();
        }
    }

    @Override
    @NotNull
    public List<E> findAll() throws AbstractException {
        try (@NotNull final Connection connection = connectionService.getConnection()) {
            @NotNull final IRepository<E> repository = getRepository(connection);
            return repository.findAll();
        } catch (SQLException exception) {
            logService.error(exception);
            throw new DatabaseOperationException();
        }
    }

    @Override
    @NotNull
    public List<E> findAll(@NotNull final Comparator<E> comparator) throws AbstractException {
        try (@NotNull final Connection connection = connectionService.getConnection()) {
            @NotNull final IRepository<E> repository = getRepository(connection);
            return repository.findAll(comparator);
        } catch (SQLException exception) {
            logService.error(exception);
            throw new DatabaseOperationException();
        }
    }

    @Override
    @NotNull
    public E findById(@NotNull final String id) throws AbstractException {
        if (EmptyUtil.isEmpty(id)) throw new EmptyIdException();
        try (@NotNull final Connection connection = connectionService.getConnection()) {
            @NotNull final IRepository<E> repository = getRepository(connection);
            return repository.findById(id);
        } catch (SQLException exception) {
            logService.error(exception);
            throw new DatabaseOperationException();
        }
    }

    @Override
    @NotNull
    public E findByIndex(final int index) throws AbstractException {
        if (index < 0) throw new IndexIncorrectException();
        try (@NotNull final Connection connection = connectionService.getConnection()) {
            @NotNull final IRepository<E> repository = getRepository(connection);
            return repository.findByIndex(index);
        } catch (SQLException exception) {
            logService.error(exception);
            throw new DatabaseOperationException();
        }
    }

    @Override
    public void removeById(@NotNull final String id) throws AbstractException {
        if (EmptyUtil.isEmpty(id)) throw new EmptyIdException();
        try (@NotNull final Connection connection = connectionService.getConnection()) {
            @NotNull final IRepository<E> repository = getRepository(connection);
            repository.removeById(id);
            connection.commit();
        } catch (SQLException exception) {
            logService.error(exception);
            throw new DatabaseOperationException();
        }
    }

    @Override
    public void removeByIndex(final int index) throws AbstractException {
        if (index < 0) throw new IndexIncorrectException();
        try (@NotNull final Connection connection = connectionService.getConnection()) {
            @NotNull final IRepository<E> repository = getRepository(connection);
            repository.removeByIndex(index);
            connection.commit();
        } catch (SQLException exception) {
            logService.error(exception);
            throw new DatabaseOperationException();
        }
    }

}
