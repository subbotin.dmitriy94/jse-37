package com.tsconsulting.dsubbotin.tm.service;

import com.tsconsulting.dsubbotin.tm.api.repository.ISessionRepository;
import com.tsconsulting.dsubbotin.tm.api.repository.IUserRepository;
import com.tsconsulting.dsubbotin.tm.api.service.IConnectionService;
import com.tsconsulting.dsubbotin.tm.api.service.ILogService;
import com.tsconsulting.dsubbotin.tm.api.service.IPropertyService;
import com.tsconsulting.dsubbotin.tm.api.service.ISessionService;
import com.tsconsulting.dsubbotin.tm.enumerated.Role;
import com.tsconsulting.dsubbotin.tm.exception.AbstractException;
import com.tsconsulting.dsubbotin.tm.exception.system.AccessDeniedException;
import com.tsconsulting.dsubbotin.tm.exception.system.DatabaseOperationException;
import com.tsconsulting.dsubbotin.tm.model.Session;
import com.tsconsulting.dsubbotin.tm.model.User;
import com.tsconsulting.dsubbotin.tm.repository.SessionRepository;
import com.tsconsulting.dsubbotin.tm.repository.UserRepository;
import com.tsconsulting.dsubbotin.tm.util.EmptyUtil;
import com.tsconsulting.dsubbotin.tm.util.HashUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Date;

public final class SessionService extends AbstractService<Session> implements ISessionService {

    public SessionService(@NotNull IConnectionService connectionService, @NotNull ILogService logService) {
        super(connectionService, logService);
    }

    @NotNull
    @Override
    protected ISessionRepository getRepository(@NotNull final Connection connection) {
        return new SessionRepository(connection);
    }

    @NotNull
    @Override
    public Session open(
            @NotNull final String login,
            @NotNull final String password
    ) throws AbstractException {
        @NotNull final Session session = new Session();
        try (@NotNull final Connection connection = connectionService.getConnection()) {
            @NotNull final IUserRepository userRepository = new UserRepository(connection);
            @NotNull final User user = userRepository.findByLogin(login);
            @NotNull final IPropertyService propertyService = new PropertyService();
            final int iteration = propertyService.getPasswordIteration();
            @NotNull final String secret = propertyService.getPasswordSecret();
            @NotNull final String hash = HashUtil.salt(iteration, secret, password);
            if (!hash.equals(user.getPasswordHash())) throw new AccessDeniedException();
            session.setUserId(user.getId());
            session.setDate(new Date());
            session.setSignature(null);
            @NotNull final ISessionRepository sessionRepository = getRepository(connection);
            sessionRepository.open(sign(session));
            connection.commit();
        } catch (SQLException exception) {
            logService.error(exception);
            throw new DatabaseOperationException();
        }
        return session;
    }

    @Override
    public boolean close(@NotNull final Session session) throws AbstractException {
        try (@NotNull final Connection connection = connectionService.getConnection()) {
            @NotNull final ISessionRepository sessionRepository = getRepository(connection);
            final boolean isClosed = sessionRepository.close(session);
            connection.commit();
            return isClosed;
        } catch (SQLException exception) {
            logService.error(exception);
            throw new DatabaseOperationException();
        }
    }

    @Override
    public void validate(
            @Nullable final Session session,
            @NotNull final Role role
    ) throws AbstractException {
        validate(session);
        try (@NotNull final Connection connection = connectionService.getConnection()) {
            if (session == null) throw new AccessDeniedException();
            @NotNull final String userId = session.getUserId();
            @NotNull final IUserRepository userRepository = new UserRepository(connection);
            @NotNull final Role userRole = userRepository.findById(userId).getRole();
            if (!userRole.equals(role)) throw new AccessDeniedException();
        } catch (SQLException exception) {
            logService.error(exception);
            throw new DatabaseOperationException();
        }
    }

    @Override
    public void validate(@Nullable final Session session) throws AbstractException {
        checkSession(session);
        @Nullable final Session sessionTemp = session.clone();
        if (sessionTemp == null) throw new AccessDeniedException();
        @NotNull final String signatureSource = session.getSignature();
        @Nullable final String signatureTarget = sign(sessionTemp).getSignature();
        if (!signatureSource.equals(signatureTarget)) throw new AccessDeniedException();
        try (@NotNull final Connection connection = connectionService.getConnection()) {
            @NotNull final ISessionRepository sessionRepository = getRepository(connection);
            if (!sessionRepository.contains(session.getId())) throw new AccessDeniedException();
        } catch (SQLException exception) {
            logService.error(exception);
            throw new DatabaseOperationException();
        }
    }

    @NotNull
    @Override
    public User getUser(@NotNull final Session session) throws AbstractException {
        @NotNull final String userId = session.getUserId();
        try (@NotNull final Connection connection = connectionService.getConnection()) {
            @NotNull final IUserRepository userRepository = new UserRepository(connection);
            return userRepository.findById(userId);
        } catch (SQLException exception) {
            logService.error(exception);
            throw new DatabaseOperationException();
        }
    }

    @NotNull
    @Override
    public String getUserId(@NotNull final Session session) {
        return session.getUserId();
    }

    @NotNull
    private Session sign(@NotNull final Session session) {
        session.setSignature(null);
        @NotNull final IPropertyService propertyService = new PropertyService();
        final int iteration = propertyService.getSignatureIteration();
        @NotNull String secret = propertyService.getSignatureSecret();
        @Nullable final String signature = HashUtil.sign(iteration, secret, session);
        session.setSignature(signature);
        return session;
    }

    private void checkSession(@Nullable final Session session) throws AccessDeniedException {
        if (session == null) throw new AccessDeniedException();
        if (EmptyUtil.isEmpty(session.getUserId())) throw new AccessDeniedException();
        if (EmptyUtil.isEmpty(session.getDate().toString())) throw new AccessDeniedException();
        if (EmptyUtil.isEmpty(session.getSignature())) throw new AccessDeniedException();
    }

}
