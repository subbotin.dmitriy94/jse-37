package com.tsconsulting.dsubbotin.tm.api.service;

import com.tsconsulting.dsubbotin.tm.enumerated.Role;
import com.tsconsulting.dsubbotin.tm.exception.AbstractException;
import com.tsconsulting.dsubbotin.tm.model.User;
import org.jetbrains.annotations.NotNull;

import java.util.List;

public interface IUserService extends IService<User> {

    User create(
            @NotNull String login,
            @NotNull String password
    ) throws AbstractException;

    User create(
            @NotNull String login,
            @NotNull String password,
            @NotNull Role role
    ) throws AbstractException;

    User create(
            @NotNull String login,
            @NotNull String password,
            @NotNull Role role,
            @NotNull String email
    ) throws AbstractException;

    void addAll(@NotNull List<User> users) throws AbstractException;

    @NotNull
    User findByLogin(@NotNull String login) throws AbstractException;

    void removeByLogin(@NotNull String login) throws AbstractException;

    void setPassword(@NotNull String id, @NotNull String password) throws AbstractException;

    void setRole(@NotNull String id, @NotNull Role role) throws AbstractException;

    void updateById(
            @NotNull String id,
            @NotNull String lastName,
            @NotNull String firstName,
            @NotNull String middleName,
            @NotNull String email
    ) throws AbstractException;

    boolean isLogin(@NotNull String login) throws AbstractException;

    void lockByLogin(@NotNull String login) throws AbstractException;

    void unlockByLogin(@NotNull String login) throws AbstractException;

}
