package com.tsconsulting.dsubbotin.tm.repository;

import com.tsconsulting.dsubbotin.tm.api.repository.ISessionRepository;
import com.tsconsulting.dsubbotin.tm.model.Session;
import org.jetbrains.annotations.NotNull;

import java.sql.*;

public class SessionRepository extends AbstractRepository<Session> implements ISessionRepository {

    public SessionRepository(@NotNull Connection connection) {
        super(connection);
    }

    @Override
    protected @NotNull Session fetch(@NotNull ResultSet row) throws SQLException {
        @NotNull final Session session = new Session();
        session.setId(row.getString("id"));
        session.setDate(row.getDate("date"));
        session.setSignature(row.getString("signature"));
        session.setUserId(row.getString("user_id"));
        return session;
    }

    @Override
    protected @NotNull String getTableName() {
        return "sessions";
    }

    @Override
    public void open(@NotNull final Session session) throws SQLException {
        @NotNull final String query = "INSERT INTO " + getTableName() +
                " (id, date, signature, user_id)" +
                " VALUES (?, ?, ?, ?);";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, session.getId());
        statement.setTimestamp(2, new Timestamp(session.getDate().getTime()));
        statement.setString(3, session.getSignature());
        statement.setString(4, session.getUserId());
        statement.executeUpdate();
        statement.close();
    }

    @Override
    public boolean close(@NotNull final Session session) throws SQLException {
        @NotNull final String query = "DELETE FROM " + getTableName() + " WHERE id = ?;";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, session.getId());
        final int count = statement.executeUpdate();
        statement.close();
        return count > 0;
    }

    @Override
    public boolean contains(@NotNull final String id) throws SQLException {
        @NotNull final String query = "SELECT * FROM " + getTableName() + " WHERE id = ?;";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, id);
        @NotNull final ResultSet resultSet = statement.executeQuery();
        final boolean found = resultSet.next();
        statement.close();
        return found;
    }

}
