package com.tsconsulting.dsubbotin.tm.api.repository;

import com.tsconsulting.dsubbotin.tm.enumerated.Role;
import com.tsconsulting.dsubbotin.tm.exception.AbstractException;
import com.tsconsulting.dsubbotin.tm.model.User;
import org.jetbrains.annotations.NotNull;

import java.sql.SQLException;

public interface IUserRepository extends IRepository<User> {

    void add(@NotNull User user) throws AbstractException, SQLException;

    @NotNull
    User findByLogin(@NotNull String login) throws AbstractException, SQLException;

    void removeByLogin(@NotNull String login) throws AbstractException, SQLException;

    void setPassword(
            @NotNull String id,
            @NotNull String passwordHash
    ) throws AbstractException, SQLException;

    void setRole(
            @NotNull String id,
            @NotNull Role role
    ) throws AbstractException, SQLException;

    void updateById(
            @NotNull String id,
            @NotNull String lastName,
            @NotNull String firstName,
            @NotNull String middleName,
            @NotNull String email
    ) throws AbstractException, SQLException;

    void lockByLogin(@NotNull String login) throws AbstractException, SQLException;

    void unlockByLogin(@NotNull String login) throws AbstractException, SQLException;

    boolean isLogin(@NotNull String login) throws AbstractException, SQLException;

}
