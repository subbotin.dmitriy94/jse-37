package com.tsconsulting.dsubbotin.tm.exception.system;

import com.tsconsulting.dsubbotin.tm.exception.AbstractException;

public final class AccessDeniedException extends AbstractException {

    public AccessDeniedException() {
        super("Access denied!");
    }

}
