package com.tsconsulting.dsubbotin.tm.repository;

import com.tsconsulting.dsubbotin.tm.api.repository.IUserRepository;
import com.tsconsulting.dsubbotin.tm.enumerated.Role;
import com.tsconsulting.dsubbotin.tm.exception.AbstractException;
import com.tsconsulting.dsubbotin.tm.exception.entity.UserNotFoundException;
import com.tsconsulting.dsubbotin.tm.model.User;
import com.tsconsulting.dsubbotin.tm.util.EnumerationUtil;
import org.jetbrains.annotations.NotNull;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public final class UserRepository extends AbstractRepository<User> implements IUserRepository {

    public UserRepository(@NotNull Connection connection) {
        super(connection);
    }

    @NotNull
    @Override
    protected User fetch(@NotNull ResultSet row) throws AbstractException, SQLException {
        @NotNull final User user = new User();
        user.setId(row.getString("id"));
        user.setLogin(row.getString("login"));
        user.setPasswordHash(row.getString("password_hash"));
        user.setEmail(row.getString("email"));
        user.setRole(EnumerationUtil.parseRole(row.getString("role")));
        user.setFirstName(row.getString("first_name"));
        user.setLastName(row.getString("last_name"));
        user.setMiddleName(row.getString("middle_name"));
        user.setLocked(row.getBoolean("locked"));
        return user;
    }

    @NotNull
    @Override
    protected String getTableName() {
        return "users";
    }

    @Override
    public void add(@NotNull final User user) throws AbstractException, SQLException {
        @NotNull final String query = "INSERT INTO " + getTableName() +
                " (id, login, password_hash, email, role, first_name, last_name, middle_name, locked)" +
                " VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?);";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, user.getId());
        statement.setString(2, user.getLogin());
        statement.setString(3, user.getPasswordHash());
        statement.setString(4, user.getEmail());
        statement.setString(5, user.getRole().toString());
        statement.setString(6, user.getFirstName());
        statement.setString(7, user.getLastName());
        statement.setString(8, user.getMiddleName());
        statement.setBoolean(9, user.isLocked());
        statement.executeUpdate();
        statement.close();
    }

    @NotNull
    @Override
    public User findByLogin(@NotNull final String login) throws AbstractException, SQLException {
        @NotNull final String query = "SELECT * FROM " + getTableName() + " WHERE login = ?;";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, login);
        @NotNull final ResultSet resultSet = statement.executeQuery();
        if (!resultSet.next()) throw new UserNotFoundException();
        @NotNull final User user = fetch(resultSet);
        statement.close();
        return user;
    }

    @Override
    public void removeByLogin(@NotNull final String login) throws SQLException {
        @NotNull final String query = "DELETE FROM " + getTableName() + " WHERE login = ?;";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, login);
        statement.executeUpdate();
        statement.close();
    }

    @Override
    public boolean isLogin(@NotNull final String login) throws AbstractException, SQLException {
        try {
            findByLogin(login);
            return true;
        } catch (UserNotFoundException exception) {
            return false;
        }
    }

    @Override
    public void setPassword(
            @NotNull final String id,
            @NotNull final String passwordHash
    ) throws SQLException {
        @NotNull final String query = "UPDATE " + getTableName() +
                " SET password_hash = ? WHERE id = ?;";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, passwordHash);
        statement.setString(2, id);
        statement.executeUpdate();
        statement.close();
    }

    @Override
    public void setRole(
            @NotNull final String id,
            @NotNull final Role role
    ) throws SQLException {
        @NotNull final String query = "UPDATE " + getTableName() + " SET role = ? WHERE id = ?;";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, role.toString());
        statement.setString(2, id);
        statement.executeUpdate();
        statement.close();
    }

    @Override
    public void updateById(
            @NotNull final String id,
            @NotNull final String lastName,
            @NotNull final String firstName,
            @NotNull final String middleName,
            @NotNull final String email
    ) throws AbstractException, SQLException {
        @NotNull final String query = "UPDATE " + getTableName() +
                " SET last_name = ?, first_name = ?, middle_name = ?, email = ? WHERE id = ?;";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, lastName);
        statement.setString(2, firstName);
        statement.setString(3, middleName);
        statement.setString(4, email);
        statement.setString(5, id);
        statement.executeUpdate();
        statement.close();
    }

    @Override
    public void lockByLogin(@NotNull final String login) throws SQLException {
        lockUnlockByLogin(login, true);
    }

    @Override
    public void unlockByLogin(@NotNull final String login) throws SQLException {
        lockUnlockByLogin(login, false);
    }

    private void lockUnlockByLogin(@NotNull final String login, final boolean locked) throws SQLException {
        @NotNull final String query = "UPDATE " + getTableName() + " SET locked = ? WHERE login = ?;";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setBoolean(1, locked);
        statement.setString(2, login);
        statement.executeUpdate();
        statement.close();
    }

}
