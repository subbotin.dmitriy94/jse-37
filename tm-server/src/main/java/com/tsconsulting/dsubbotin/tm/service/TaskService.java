package com.tsconsulting.dsubbotin.tm.service;

import com.tsconsulting.dsubbotin.tm.api.repository.ITaskRepository;
import com.tsconsulting.dsubbotin.tm.api.service.IConnectionService;
import com.tsconsulting.dsubbotin.tm.api.service.ILogService;
import com.tsconsulting.dsubbotin.tm.api.service.ITaskService;
import com.tsconsulting.dsubbotin.tm.enumerated.Status;
import com.tsconsulting.dsubbotin.tm.exception.AbstractException;
import com.tsconsulting.dsubbotin.tm.exception.empty.EmptyDescriptionException;
import com.tsconsulting.dsubbotin.tm.exception.empty.EmptyIdException;
import com.tsconsulting.dsubbotin.tm.exception.empty.EmptyNameException;
import com.tsconsulting.dsubbotin.tm.exception.system.DatabaseOperationException;
import com.tsconsulting.dsubbotin.tm.exception.system.IndexIncorrectException;
import com.tsconsulting.dsubbotin.tm.model.Task;
import com.tsconsulting.dsubbotin.tm.repository.TaskRepository;
import com.tsconsulting.dsubbotin.tm.util.EmptyUtil;
import org.jetbrains.annotations.NotNull;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

public final class TaskService extends AbstractOwnerService<Task> implements ITaskService {

    public TaskService(@NotNull IConnectionService connectionService, @NotNull ILogService logService) {
        super(connectionService, logService);
    }

    @NotNull
    @Override
    public ITaskRepository getRepository(@NotNull final Connection connection) {
        return new TaskRepository(connection);
    }

    @NotNull
    @Override
    public Task create(
            @NotNull final String userId,
            @NotNull final String name,
            @NotNull final String description
    ) throws AbstractException {
        checkName(name);
        if (EmptyUtil.isEmpty(description)) throw new EmptyDescriptionException();
        @NotNull Task task = new Task();
        task.setName(name);
        task.setDescription(description);
        task.setUserId(userId);
        try (@NotNull final Connection connection = connectionService.getConnection()) {
            @NotNull final ITaskRepository taskRepository = getRepository(connection);
            taskRepository.add(userId, task);
            connection.commit();
        } catch (SQLException exception) {
            logService.error(exception);
            throw new DatabaseOperationException();
        }
        return task;
    }

    @Override
    public void addAll(@NotNull List<Task> tasks) throws AbstractException {
        try (@NotNull final Connection connection = connectionService.getConnection()) {
            @NotNull final ITaskRepository taskRepository = getRepository(connection);
            for (@NotNull final Task task : tasks) taskRepository.add(task.getUserId(), task);
            connection.commit();
        } catch (SQLException exception) {
            logService.error(exception);
            throw new DatabaseOperationException();
        }
    }

    @Override
    public void clear(@NotNull final String userId) throws AbstractException {
        try (@NotNull final Connection connection = connectionService.getConnection()) {
            @NotNull final ITaskRepository taskRepository = getRepository(connection);
            taskRepository.clear(userId);
            connection.commit();
        } catch (SQLException exception) {
            logService.error(exception);
            throw new DatabaseOperationException();
        }
    }

    @Override
    @NotNull
    public Task findByName(
            @NotNull final String userId,
            @NotNull final String name
    ) throws AbstractException {
        checkName(name);
        try (@NotNull final Connection connection = connectionService.getConnection()) {
            @NotNull final ITaskRepository taskRepository = getRepository(connection);
            return taskRepository.findByName(userId, name);
        } catch (SQLException exception) {
            logService.error(exception);
            throw new DatabaseOperationException();
        }
    }

    @Override
    public void removeByName(
            @NotNull final String userId,
            @NotNull final String name
    ) throws AbstractException {
        checkName(name);
        try (@NotNull final Connection connection = connectionService.getConnection()) {
            @NotNull final ITaskRepository taskRepository = getRepository(connection);
            taskRepository.removeByName(userId, name);
            connection.commit();
        } catch (SQLException exception) {
            logService.error(exception);
            throw new DatabaseOperationException();
        }
    }

    @Override
    public void updateById(
            @NotNull final String userId,
            @NotNull final String id,
            @NotNull final String name,
            @NotNull final String description
    ) throws AbstractException {
        checkId(id);
        checkName(name);
        try (@NotNull final Connection connection = connectionService.getConnection()) {
            @NotNull final ITaskRepository taskRepository = getRepository(connection);
            taskRepository.updateById(userId, id, name, description);
            connection.commit();
        } catch (SQLException exception) {
            logService.error(exception);
            throw new DatabaseOperationException();
        }
    }

    @Override
    public void updateByIndex(
            @NotNull final String userId,
            final int index,
            @NotNull final String name,
            @NotNull final String description
    ) throws AbstractException {
        checkIndex(index);
        checkName(name);
        try (@NotNull final Connection connection = connectionService.getConnection()) {
            @NotNull final ITaskRepository taskRepository = getRepository(connection);
            taskRepository.updateByIndex(userId, index, name, description);
            connection.commit();
        } catch (SQLException exception) {
            logService.error(exception);
            throw new DatabaseOperationException();
        }
    }

    @Override
    public void startById(
            @NotNull final String userId,
            @NotNull final String id
    ) throws AbstractException {
        checkId(id);
        try (@NotNull final Connection connection = connectionService.getConnection()) {
            @NotNull final ITaskRepository taskRepository = getRepository(connection);
            taskRepository.startById(userId, id);
            connection.commit();
        } catch (SQLException exception) {
            logService.error(exception);
            throw new DatabaseOperationException();
        }
    }

    @Override
    public void startByIndex(
            @NotNull final String userId,
            final int index
    ) throws AbstractException {
        checkIndex(index);
        try (@NotNull final Connection connection = connectionService.getConnection()) {
            @NotNull final ITaskRepository taskRepository = getRepository(connection);
            taskRepository.startByIndex(userId, index);
            connection.commit();
        } catch (SQLException exception) {
            logService.error(exception);
            throw new DatabaseOperationException();
        }
    }

    @Override
    public void startByName(
            @NotNull final String userId,
            @NotNull final String name
    ) throws AbstractException {
        checkName(name);
        try (@NotNull final Connection connection = connectionService.getConnection()) {
            @NotNull final ITaskRepository taskRepository = getRepository(connection);
            taskRepository.startByName(userId, name);
            connection.commit();
        } catch (SQLException exception) {
            logService.error(exception);
            throw new DatabaseOperationException();
        }
    }

    @Override
    public void finishById(
            @NotNull final String userId,
            @NotNull final String id
    ) throws AbstractException {
        checkId(id);
        try (@NotNull final Connection connection = connectionService.getConnection()) {
            @NotNull final ITaskRepository taskRepository = getRepository(connection);
            taskRepository.finishById(userId, id);
            connection.commit();
        } catch (SQLException exception) {
            logService.error(exception);
            throw new DatabaseOperationException();
        }
    }

    @Override
    public void finishByIndex(
            @NotNull final String userId,
            final int index
    ) throws AbstractException {
        checkIndex(index);
        try (@NotNull final Connection connection = connectionService.getConnection()) {
            @NotNull final ITaskRepository taskRepository = getRepository(connection);
            taskRepository.finishByIndex(userId, index);
            connection.commit();
        } catch (SQLException exception) {
            logService.error(exception);
            throw new DatabaseOperationException();
        }
    }

    @Override
    public void finishByName(
            @NotNull final String userId,
            @NotNull final String name
    ) throws AbstractException {
        checkName(name);
        try (@NotNull final Connection connection = connectionService.getConnection()) {
            @NotNull final ITaskRepository taskRepository = getRepository(connection);
            taskRepository.finishByName(userId, name);
            connection.commit();
        } catch (SQLException exception) {
            logService.error(exception);
            throw new DatabaseOperationException();
        }
    }

    @Override
    public void updateStatusById(
            @NotNull final String userId,
            @NotNull final String id,
            @NotNull final Status status
    ) throws AbstractException {
        checkId(id);
        try (@NotNull final Connection connection = connectionService.getConnection()) {
            @NotNull final ITaskRepository taskRepository = getRepository(connection);
            taskRepository.updateStatusById(userId, id, status);
            connection.commit();
        } catch (SQLException exception) {
            logService.error(exception);
            throw new DatabaseOperationException();
        }
    }

    @Override
    public void updateStatusByIndex(
            @NotNull final String userId,
            final int index,
            @NotNull final Status status
    ) throws AbstractException {
        checkIndex(index);
        try (@NotNull final Connection connection = connectionService.getConnection()) {
            @NotNull final ITaskRepository taskRepository = getRepository(connection);
            taskRepository.updateStatusByIndex(userId, index, status);
            connection.commit();
        } catch (SQLException exception) {
            logService.error(exception);
            throw new DatabaseOperationException();
        }
    }

    @Override
    public void updateStatusByName(
            @NotNull final String userId,
            @NotNull final String name,
            @NotNull final Status status
    ) throws AbstractException {
        checkName(name);
        try (@NotNull final Connection connection = connectionService.getConnection()) {
            @NotNull final ITaskRepository taskRepository = getRepository(connection);
            taskRepository.updateStatusByName(userId, name, status);
            connection.commit();
        } catch (SQLException exception) {
            logService.error(exception);
            throw new DatabaseOperationException();
        }
    }

    private void checkName(@NotNull final String name) throws EmptyNameException {
        if (EmptyUtil.isEmpty(name)) throw new EmptyNameException();
    }

    private void checkId(@NotNull final String id) throws EmptyIdException {
        if (EmptyUtil.isEmpty(id)) throw new EmptyIdException();
    }

    private void checkIndex(final int index) throws IndexIncorrectException {
        if (index < 0) throw new IndexIncorrectException();
    }

}