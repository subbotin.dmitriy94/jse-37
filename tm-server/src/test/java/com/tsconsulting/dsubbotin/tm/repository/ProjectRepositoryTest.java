package com.tsconsulting.dsubbotin.tm.repository;

import com.tsconsulting.dsubbotin.tm.api.repository.IProjectRepository;
import com.tsconsulting.dsubbotin.tm.api.repository.IUserRepository;
import com.tsconsulting.dsubbotin.tm.api.service.IConnectionService;
import com.tsconsulting.dsubbotin.tm.api.service.IPropertyService;
import com.tsconsulting.dsubbotin.tm.enumerated.Status;
import com.tsconsulting.dsubbotin.tm.exception.AbstractException;
import com.tsconsulting.dsubbotin.tm.model.Project;
import com.tsconsulting.dsubbotin.tm.model.User;
import com.tsconsulting.dsubbotin.tm.service.ConnectionService;
import com.tsconsulting.dsubbotin.tm.service.PropertyService;
import com.tsconsulting.dsubbotin.tm.util.HashUtil;
import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.sql.Connection;
import java.sql.SQLException;

public final class ProjectRepositoryTest {

    @NotNull
    private final IProjectRepository projectRepository;

    @NotNull
    private final IUserRepository userRepository;

    @NotNull
    private final Project project;

    @NotNull
    private final String projectId;

    @NotNull
    private final String projectName = "testProject";

    @NotNull
    private final String projectDescription = "testProject";

    @NotNull
    private final String userId;

    @NotNull
    private final Connection connection;

    public ProjectRepositoryTest() throws SQLException, AbstractException {
        @NotNull final IPropertyService propertyService = new PropertyService();
        @NotNull final IConnectionService connectionService = new ConnectionService(propertyService);
        connection = connectionService.getConnection();
        projectRepository = new ProjectRepository(connection);
        userRepository = new UserRepository(connection);
        @NotNull final User user = new User();
        userId = user.getId();
        user.setLogin("guest");
        user.setPasswordHash(HashUtil.salt(3, "qwe", "guest"));
        userRepository.add(user);
        project = new Project();
        projectId = project.getId();
        project.setUserId(userId);
        project.setName(projectName);
        project.setDescription(projectDescription);
        connection.commit();
    }

    @Before
    public void initializeTest() throws AbstractException, SQLException {
        projectRepository.add(userId, project);
        connection.commit();
    }

    @Test
    public void findProject() throws AbstractException, SQLException {
        checkProject(projectRepository.findByName(userId, projectName));
        checkProject(projectRepository.findById(projectId));
        checkProject(projectRepository.findById(userId, projectId));
        checkProject(projectRepository.findByIndex(0));
    }

    private void checkProject(@NotNull final Project foundProject) {
        Assert.assertEquals(project.getId(), foundProject.getId());
        Assert.assertEquals(project.getName(), foundProject.getName());
        Assert.assertEquals(project.getDescription(), foundProject.getDescription());
        Assert.assertEquals(project.getUserId(), foundProject.getUserId());
        Assert.assertEquals(project.getStartDate(), foundProject.getStartDate());
    }

    @Test
    public void removeByName() throws AbstractException, SQLException {
        Assert.assertNotNull(project);
        projectRepository.removeByName(userId, projectName);
        connection.commit();
        Assert.assertTrue(projectRepository.findAll().isEmpty());
    }

    @Test
    public void updateById() throws AbstractException, SQLException {
        @NotNull final String name = "projectNameUpd";
        @NotNull final String description = "projectNDescriptionUpd";
        projectRepository.updateById(userId, projectId, name, description);
        connection.commit();
        Assert.assertEquals(name, projectRepository.findById(userId, projectId).getName());
        Assert.assertEquals(description, projectRepository.findById(userId, projectId).getDescription());
        Assert.assertNotEquals(projectName, projectRepository.findById(userId, projectId).getName());
        Assert.assertNotEquals(projectDescription, projectRepository.findById(userId, projectId).getDescription());
    }

    @Test
    public void updateByIndex() throws AbstractException, SQLException {
        @NotNull final String name = "projectNameUpd";
        @NotNull final String description = "projectNDescriptionUpd";
        projectRepository.updateByIndex(userId, 0, name, description);
        connection.commit();
        Assert.assertEquals(name, projectRepository.findById(userId, projectId).getName());
        Assert.assertEquals(description, projectRepository.findById(userId, projectId).getDescription());
        Assert.assertNotEquals(projectName, projectRepository.findById(userId, projectId).getName());
        Assert.assertNotEquals(projectDescription, projectRepository.findById(userId, projectId).getDescription());
    }

    @Test
    public void startById() throws AbstractException, SQLException {
        projectRepository.startById(userId, projectId);
        connection.commit();
        Assert.assertEquals(projectRepository.findById(userId, projectId).getStatus(), Status.IN_PROGRESS);
    }

    @Test
    public void startByIndex() throws AbstractException, SQLException {
        projectRepository.startByIndex(userId, 0);
        connection.commit();
        Assert.assertEquals(projectRepository.findByIndex(userId, 0).getStatus(), Status.IN_PROGRESS);
    }

    @Test
    public void startByName() throws AbstractException, SQLException {
        projectRepository.startByName(userId, projectName);
        connection.commit();
        Assert.assertEquals(projectRepository.findByName(userId, projectName).getStatus(), Status.IN_PROGRESS);
    }

    @Test
    public void finishById() throws AbstractException, SQLException {
        projectRepository.finishById(userId, projectId);
        connection.commit();
        Assert.assertEquals(projectRepository.findById(userId, projectId).getStatus(), Status.COMPLETED);
    }

    @Test
    public void finishByIndex() throws AbstractException, SQLException {
        projectRepository.finishByIndex(userId, 0);
        connection.commit();
        Assert.assertEquals(projectRepository.findByIndex(userId, 0).getStatus(), Status.COMPLETED);
    }

    @Test
    public void finishByName() throws AbstractException, SQLException {
        projectRepository.finishByName(userId, projectName);
        connection.commit();
        Assert.assertEquals(projectRepository.findByName(userId, projectName).getStatus(), Status.COMPLETED);
    }

    @Test
    public void updateStatusById() throws AbstractException, SQLException {
        projectRepository.updateStatusById(userId, projectId, Status.IN_PROGRESS);
        connection.commit();
        Assert.assertEquals(projectRepository.findById(userId, projectId).getStatus(), Status.IN_PROGRESS);
        projectRepository.updateStatusById(userId, projectId, Status.COMPLETED);
        connection.commit();
        Assert.assertEquals(projectRepository.findById(userId, projectId).getStatus(), Status.COMPLETED);
        projectRepository.updateStatusById(userId, projectId, Status.NOT_STARTED);
        connection.commit();
        Assert.assertEquals(projectRepository.findById(userId, projectId).getStatus(), Status.NOT_STARTED);
    }

    @Test
    public void updateStatusByIndex() throws AbstractException, SQLException {
        projectRepository.updateStatusByIndex(userId, 0, Status.IN_PROGRESS);
        connection.commit();
        Assert.assertEquals(projectRepository.findByIndex(userId, 0).getStatus(), Status.IN_PROGRESS);
        projectRepository.updateStatusByIndex(userId, 0, Status.COMPLETED);
        connection.commit();
        Assert.assertEquals(projectRepository.findByIndex(userId, 0).getStatus(), Status.COMPLETED);
        projectRepository.updateStatusByIndex(userId, 0, Status.NOT_STARTED);
        connection.commit();
        Assert.assertEquals(projectRepository.findByIndex(userId, 0).getStatus(), Status.NOT_STARTED);
    }

    @Test
    public void updateStatusByName() throws AbstractException, SQLException {
        projectRepository.updateStatusByName(userId, projectName, Status.IN_PROGRESS);
        connection.commit();
        Assert.assertEquals(projectRepository.findByName(userId, projectName).getStatus(), Status.IN_PROGRESS);
        projectRepository.updateStatusByName(userId, projectName, Status.COMPLETED);
        connection.commit();
        Assert.assertEquals(projectRepository.findByName(userId, projectName).getStatus(), Status.COMPLETED);
        projectRepository.updateStatusByName(userId, projectName, Status.NOT_STARTED);
        connection.commit();
        Assert.assertEquals(projectRepository.findByName(userId, projectName).getStatus(), Status.NOT_STARTED);
    }

    @After
    public void finalizeTest() throws SQLException, AbstractException {
        projectRepository.clear(userId);
        userRepository.removeById(userId);
        connection.commit();
        connection.close();
    }

}