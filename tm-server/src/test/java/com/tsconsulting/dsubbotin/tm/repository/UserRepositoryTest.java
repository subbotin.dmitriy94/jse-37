package com.tsconsulting.dsubbotin.tm.repository;

import com.tsconsulting.dsubbotin.tm.api.repository.IUserRepository;
import com.tsconsulting.dsubbotin.tm.api.service.IConnectionService;
import com.tsconsulting.dsubbotin.tm.api.service.IPropertyService;
import com.tsconsulting.dsubbotin.tm.exception.AbstractException;
import com.tsconsulting.dsubbotin.tm.model.User;
import com.tsconsulting.dsubbotin.tm.service.ConnectionService;
import com.tsconsulting.dsubbotin.tm.service.PropertyService;
import com.tsconsulting.dsubbotin.tm.util.HashUtil;
import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.sql.Connection;
import java.sql.SQLException;

public final class UserRepositoryTest {

    @NotNull
    private final IUserRepository userRepository;

    @NotNull
    private final User user;

    @NotNull
    private final String userLogin = "userTest";

    @NotNull
    private final Connection connection;

    public UserRepositoryTest() {
        @NotNull final IPropertyService propertyService = new PropertyService();
        @NotNull final IConnectionService connectionService = new ConnectionService(propertyService);
        connection = connectionService.getConnection();
        userRepository = new UserRepository(connection);
        user = new User();
        user.setLogin(userLogin);
        @NotNull final String password = "userTest";
        @NotNull final String secret = propertyService.getPasswordSecret();
        final int iteration = propertyService.getPasswordIteration();
        user.setPasswordHash(HashUtil.salt(iteration, secret, password));
    }

    @Before
    public void initializeTest() throws AbstractException, SQLException {
        userRepository.add(user);
        connection.commit();
    }

    @Test
    public void findByLogin() throws AbstractException, SQLException {
        @NotNull final User foundUser = userRepository.findByLogin(userLogin);
        Assert.assertEquals(user.getId(), foundUser.getId());
        Assert.assertEquals(user.getLogin(), foundUser.getLogin());
        Assert.assertEquals(user.getPasswordHash(), foundUser.getPasswordHash());
        Assert.assertEquals(user.getRole(), foundUser.getRole());
    }

    @Test
    public void removeByLogin() throws AbstractException, SQLException {
        userRepository.removeByLogin(userLogin);
        connection.commit();
        Assert.assertTrue(userRepository.findAll().isEmpty());
    }

    @Test
    public void isLogin() throws SQLException, AbstractException {
        Assert.assertTrue(userRepository.isLogin(userLogin));
    }

    @After
    public void finalizeTest() throws SQLException, AbstractException {
        userRepository.removeById(user.getId());
        connection.commit();
    }

}
