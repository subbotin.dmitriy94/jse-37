package com.tsconsulting.dsubbotin.tm.repository;

import com.tsconsulting.dsubbotin.tm.api.repository.ISessionRepository;
import com.tsconsulting.dsubbotin.tm.api.repository.IUserRepository;
import com.tsconsulting.dsubbotin.tm.api.service.IConnectionService;
import com.tsconsulting.dsubbotin.tm.api.service.IPropertyService;
import com.tsconsulting.dsubbotin.tm.exception.AbstractException;
import com.tsconsulting.dsubbotin.tm.model.Session;
import com.tsconsulting.dsubbotin.tm.model.User;
import com.tsconsulting.dsubbotin.tm.service.ConnectionService;
import com.tsconsulting.dsubbotin.tm.service.PropertyService;
import com.tsconsulting.dsubbotin.tm.util.HashUtil;
import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Date;

public final class SessionRepositoryTest {

    @NotNull
    private final ISessionRepository sessionRepository;

    @NotNull
    private final IUserRepository userRepository;

    @NotNull
    private final Session session;

    @NotNull
    private final String sessionId;

    @NotNull
    private final Connection connection;

    public SessionRepositoryTest() throws AbstractException, SQLException {
        @NotNull final IPropertyService propertyService = new PropertyService();
        @NotNull final IConnectionService connectionService = new ConnectionService(propertyService);
        connection = connectionService.getConnection();
        userRepository = new UserRepository(connection);

        @NotNull final User user = new User();
        user.setLogin("guest");
        @NotNull final String password = "guest";
        @NotNull final String secret = propertyService.getPasswordSecret();
        final int iteration = propertyService.getPasswordIteration();
        user.setPasswordHash(HashUtil.salt(iteration, secret, password));
        userRepository.add(user);
        connection.commit();
        sessionRepository = new SessionRepository(connection);
        session = new Session();
        sessionId = session.getId();
        session.setUserId(user.getId());
        session.setDate(new Date());
    }

    @Before
    public void initializeTest() {
    }

    @Test
    public void openClose() throws AbstractException, SQLException {
        sessionRepository.open(session);
        connection.commit();
        @NotNull final Session foundSession = sessionRepository.findById(sessionId);
        Assert.assertEquals(session.getId(), foundSession.getId());
        Assert.assertEquals(session.getUserId(), foundSession.getUserId());
        Assert.assertEquals(session.getSignature(), foundSession.getSignature());
        sessionRepository.close(session);
        connection.commit();
        Assert.assertTrue(sessionRepository.findAll().isEmpty());
    }

    @Test
    public void contains() throws SQLException {
        sessionRepository.open(session);
        connection.commit();
        Assert.assertTrue(sessionRepository.contains(sessionId));
    }

    @After
    public void finalizeTest() throws SQLException, AbstractException {
        sessionRepository.clear();
        userRepository.removeById(session.getUserId());
        connection.commit();
    }

}
