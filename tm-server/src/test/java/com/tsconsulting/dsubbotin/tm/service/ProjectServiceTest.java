package com.tsconsulting.dsubbotin.tm.service;

import com.tsconsulting.dsubbotin.tm.api.service.IProjectService;
import com.tsconsulting.dsubbotin.tm.api.service.IUserService;
import com.tsconsulting.dsubbotin.tm.enumerated.Status;
import com.tsconsulting.dsubbotin.tm.exception.AbstractException;
import com.tsconsulting.dsubbotin.tm.model.Project;
import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Test;

public final class ProjectServiceTest {

    @NotNull
    private final IProjectService projectService;

    @NotNull
    private final IUserService userService;

    @NotNull
    private final String projectId;

    @NotNull
    private final String projectName = "projectName";

    @NotNull
    private final String projectDescription = "projectDescription";

    @NotNull
    private final String userId;

    public ProjectServiceTest() throws AbstractException {
        projectService = new ProjectService(new ConnectionService(new PropertyService()), new LogService());
        userService = new UserService(new ConnectionService(new PropertyService()), new LogService());
        userId = userService.create("user", "user").getId();
        projectId = projectService.create(userId, projectName, projectDescription).getId();
    }

    @Test
    public void create() throws AbstractException {
        @NotNull final String newProjectName = "newProjectName";
        @NotNull final String newProjectDescription = "newProjectDescription";
        @NotNull final Project newProject = projectService.create(userId, newProjectName, newProjectDescription);
        Assert.assertEquals(2, projectService.findAll().size());
        Assert.assertEquals(newProject.getName(), newProjectName);
        Assert.assertEquals(newProject.getDescription(), newProjectDescription);
        projectService.removeById(newProject.getId());
    }

    @Test
    public void findByName() throws AbstractException {
        @NotNull final Project project = projectService.findByName(userId, projectName);
        Assert.assertEquals(project.getName(), projectName);
        Assert.assertEquals(project.getDescription(), projectDescription);
        Assert.assertEquals(project.getUserId(), userId);
    }

    @Test
    public void updateById() throws AbstractException {
        @NotNull final String newProjectName = "newProjectName";
        @NotNull final String newProjectDescription = "newProjectDescription";
        projectService.updateById(userId, projectId, newProjectName, newProjectDescription);
        @NotNull final Project tempProject = projectService.findByName(userId, newProjectName);
        Assert.assertNotEquals(tempProject.getName(), projectName);
        Assert.assertNotEquals(tempProject.getDescription(), projectDescription);
        Assert.assertEquals(tempProject.getName(), newProjectName);
        Assert.assertEquals(tempProject.getDescription(), newProjectDescription);
        Assert.assertEquals(tempProject.getId(), projectId);
        Assert.assertEquals(tempProject.getUserId(), userId);
    }

    @Test
    public void updateByIndex() throws AbstractException {
        @NotNull final String newProjectName = "newProjectName";
        @NotNull final String newProjectDescription = "newProjectDescription";
        projectService.updateByIndex(userId, 0, newProjectName, newProjectDescription);
        @NotNull final Project tempProject = projectService.findByName(userId, newProjectName);
        Assert.assertNotEquals(tempProject.getName(), projectName);
        Assert.assertNotEquals(tempProject.getDescription(), projectDescription);
        Assert.assertEquals(tempProject.getName(), newProjectName);
        Assert.assertEquals(tempProject.getDescription(), newProjectDescription);
        Assert.assertEquals(tempProject.getId(), projectId);
        Assert.assertEquals(tempProject.getUserId(), userId);
    }

    @Test
    public void startById() throws AbstractException {
        @NotNull final Project tempProject = projectService.findById(userId, projectId);
        Assert.assertNull(tempProject.getStartDate());
        Assert.assertEquals(tempProject.getStatus(), Status.NOT_STARTED);
        projectService.startById(userId, projectId);
        @NotNull final Project updProject = projectService.findById(userId, projectId);
        Assert.assertNotNull(updProject.getStartDate());
        Assert.assertEquals(updProject.getStatus(), Status.IN_PROGRESS);
    }

    @Test
    public void startByIndex() throws AbstractException {
        Assert.assertNull(projectService.findByIndex(userId, 0).getStartDate());
        Assert.assertEquals(projectService.findByIndex(userId, 0).getStatus(), Status.NOT_STARTED);
        projectService.startByIndex(userId, 0);
        Assert.assertNotNull(projectService.findByIndex(userId, 0).getStartDate());
        Assert.assertEquals(projectService.findByIndex(userId, 0).getStatus(), Status.IN_PROGRESS);
    }

    @Test
    public void startByName() throws AbstractException {
        @NotNull final Project tempProject = projectService.findByName(userId, projectName);
        Assert.assertNull(tempProject.getStartDate());
        Assert.assertEquals(tempProject.getStatus(), Status.NOT_STARTED);
        projectService.startByName(userId, projectName);
        @NotNull final Project updTempProject = projectService.findByName(userId, projectName);
        Assert.assertNotNull(updTempProject.getStartDate());
        Assert.assertEquals(updTempProject.getStatus(), Status.IN_PROGRESS);
    }

    @Test
    public void finishById() throws AbstractException {
        Assert.assertEquals(projectService.findById(userId, projectId).getStatus(), Status.NOT_STARTED);
        projectService.finishById(userId, projectId);
        Assert.assertEquals(projectService.findById(userId, projectId).getStatus(), Status.COMPLETED);
    }

    @Test
    public void finishByIndex() throws AbstractException {
        Assert.assertEquals(projectService.findByIndex(userId, 0).getStatus(), Status.NOT_STARTED);
        projectService.finishByIndex(userId, 0);
        Assert.assertEquals(projectService.findByIndex(userId, 0).getStatus(), Status.COMPLETED);
    }

    @Test
    public void finishByName() throws AbstractException {
        Assert.assertEquals(projectService.findByName(userId, projectName).getStatus(), Status.NOT_STARTED);
        projectService.finishByName(userId, projectName);
        Assert.assertEquals(projectService.findByName(userId, projectName).getStatus(), Status.COMPLETED);
    }

    @Test
    public void updateStatusById() throws AbstractException {
        Assert.assertEquals(projectService.findById(userId, projectId).getStatus(), Status.NOT_STARTED);
        projectService.updateStatusById(userId, projectId, Status.IN_PROGRESS);
        Assert.assertEquals(projectService.findById(userId, projectId).getStatus(), Status.IN_PROGRESS);
        projectService.updateStatusById(userId, projectId, Status.COMPLETED);
        Assert.assertEquals(projectService.findById(userId, projectId).getStatus(), Status.COMPLETED);
        projectService.updateStatusById(userId, projectId, Status.NOT_STARTED);
        Assert.assertEquals(projectService.findById(userId, projectId).getStatus(), Status.NOT_STARTED);
    }

    @Test
    public void updateStatusByIndex() throws AbstractException {
        Assert.assertEquals(projectService.findByIndex(userId, 0).getStatus(), Status.NOT_STARTED);
        projectService.updateStatusByIndex(userId, 0, Status.IN_PROGRESS);
        Assert.assertEquals(projectService.findByIndex(userId, 0).getStatus(), Status.IN_PROGRESS);
        projectService.updateStatusByIndex(userId, 0, Status.COMPLETED);
        Assert.assertEquals(projectService.findByIndex(userId, 0).getStatus(), Status.COMPLETED);
        projectService.updateStatusByIndex(userId, 0, Status.NOT_STARTED);
        Assert.assertEquals(projectService.findByIndex(userId, 0).getStatus(), Status.NOT_STARTED);
    }

    @Test
    public void updateStatusByName() throws AbstractException {
        Assert.assertEquals(projectService.findByName(userId, projectName).getStatus(), Status.NOT_STARTED);
        projectService.updateStatusByName(userId, projectName, Status.IN_PROGRESS);
        Assert.assertEquals(projectService.findByName(userId, projectName).getStatus(), Status.IN_PROGRESS);
        projectService.updateStatusByName(userId, projectName, Status.COMPLETED);
        Assert.assertEquals(projectService.findByName(userId, projectName).getStatus(), Status.COMPLETED);
        projectService.updateStatusByName(userId, projectName, Status.NOT_STARTED);
        Assert.assertEquals(projectService.findByName(userId, projectName).getStatus(), Status.NOT_STARTED);
    }

    @After
    public void finalizeTest() throws AbstractException {
        projectService.removeById(projectId);
        userService.removeById(userId);
    }

}
